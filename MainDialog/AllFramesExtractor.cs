﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.IO;

using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.Util;
using Ionic.Zip;
using System.Globalization;
using System.Text.RegularExpressions;

//alias
using Capture = Emgu.CV.Capture;
using System.Diagnostics;

namespace MainDialog
{
    class AllFramesExtractor
    {
        private const double MILLISECS_PER_SEC = 1000;

        /// <summary>
        /// FPS of the videos. Defaults to 20
        /// </summary>
        public double FPS { get; set; }

        /// <summary>
        /// Year of recording. Defaults to current year
        /// </summary>
        public int YearOfRecording { get; set; }

        /// <summary>
        /// The format of the saved images. Defaults to Jpeg
        /// </summary>
        public ImageFormat ImageSaveFormat { get; set; }

        /// <summary>
        /// The interval of which frames are zipped. Defaults to 5 Minutes
        /// </summary>
        public ZipInterval SaveInterval { get; set; }

        private string[] dateFormat_;

        /// <summary>
        /// The format used to read the timestamps from the input filenames. Defaults to MM-dd-HH
        /// </summary>
        public string[] DateFormat
        {
            get { return dateFormat_; }
            set { dateFormat_ = value; }
        }

        private string[] videos;
        private CancellationTokenSource tokenSource = new CancellationTokenSource();

        public AllFramesExtractor(List<string> videoFiles)
        {
            // Defaults
            FPS = 0;
            YearOfRecording = DateTime.Now.Year;
            ImageSaveFormat = ImageFormat.Jpeg;
            SaveInterval = ZipInterval.minutes_5();
            DateFormat = new string[] { @"MM-dd-HH", "M-dd-HH", "M-d-HH", "MM-d-HH" };

            // Remove files that does not exist
            var existingFiles = new List<string>();

            foreach (var file in videoFiles)
            {
                if (File.Exists(file)) existingFiles.Add(file);
            }

            videos = existingFiles.ToArray();
        }

        public void Start()
        {
            if (videos.Length == 0) throw new ArgumentException(@"No files to work on");

            bool datetest = TestDateFormatOnFile(videos[0]);

            if (!datetest) throw new ArgumentException(@"The date format did not match the files");

            Task.Factory.StartNew(() => ProcessMethod(tokenSource.Token));
        }

        public void Stop()
        {
            tokenSource.Cancel();
        }

        private void ProcessMethod(CancellationToken ct)
        {
            Debug.WriteLine("Process Method Started");

            OnProgressChanged(new ProgressEventArgs { Progress = 0, Running = true });
            int intervalseconds = SaveInterval.Seconds;
            ImageFormat imageformat = ImageSaveFormat;
            string imageExtension = imageformat.ToString();
            if (imageformat.Equals(ImageFormat.Jpeg)) imageExtension = "jpg"; // required by program developed by Lund
            int year = YearOfRecording;
            string[] dateformat = DateFormat;

            foreach (var video in videos)
            {
                using (Capture cap = new Capture(video))
                {
                    double fps = cap.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps);

                    DateTime videoStartTime = new DateTime();
                    bool succes = TryGetDateTimeFromFilename(video, year, dateformat, out videoStartTime);

                    DateTime videotime = videoStartTime;
                    Debug.WriteLine("TryGetDateTimeFromFilename: " + video + " is " + succes);
                    if (!succes) continue;

                    //var videosavepath = Path.Combine(Path.GetDirectoryName(video), "frames", videotime.ToString("yyyyMMdd-HH"));
                    var videosavepath = Path.Combine(Path.GetDirectoryName(video), "frames");
                    
                    Directory.CreateDirectory(videosavepath);

                    var fullsavepath = Path.Combine(videosavepath, videotime.ToString("yyyyMMdd-HHmmss"));
                    if (!Directory.Exists(fullsavepath)) Directory.CreateDirectory(fullsavepath);

                    DateTime stopintervaltime = videotime.AddSeconds(intervalseconds);
                    long frameNbr = 0;

                    while (cap.Grab())
                    {
                        if (ct.IsCancellationRequested)
                        {
                            OnProgressChanged(new ProgressEventArgs { Progress = 0, Running = false });
                            return;
                        }

                        if (videotime >= stopintervaltime)
                        {
                            // make zip file
                            var zipImages = Directory.GetFiles(fullsavepath);
                            ZipFiles(zipImages, fullsavepath);

                            // delete temp directory
                            Directory.Delete(fullsavepath, true);

                            // update interval and save path
                            stopintervaltime = stopintervaltime.AddSeconds(intervalseconds);
                            fullsavepath = Path.Combine(videosavepath, videotime.ToString("yyyyMMdd-HHmmss"));
                            if (!Directory.Exists(fullsavepath)) Directory.CreateDirectory(fullsavepath);
                        }

                        using (Mat image = cap.QueryFrame())
                        {
                            string filename = Path.Combine(fullsavepath, videotime.ToString("yyyyMMdd-HHmmss.fff") + "." + imageExtension);
                            image.Save(filename);
                        }

                        frameNbr++;
                        videotime = videoStartTime.AddMilliseconds((frameNbr * 1000) / fps);
                    }

                    if (Directory.Exists(fullsavepath))
                    {
                        // make zip file
                        var zipImages = Directory.GetFiles(fullsavepath);
                        ZipFiles(zipImages, fullsavepath);

                        // delete temp directory
                        Directory.Delete(fullsavepath, true);
                    }
                }
            }

            OnProgressChanged(new ProgressEventArgs { Progress = 100, Running = false });

            Debug.WriteLine("Process Method Ended");
        }

        private void ZipFiles(string[] files, string savename)
        {
            using (ZipFile zipfile = new ZipFile(Path.GetFileNameWithoutExtension(savename)))
            {
                zipfile.AddFiles(files, "");
                zipfile.Save(savename + ".zip");
            }
        }

        private bool TestDateFormatOnFile(string file)
        {
            string safefile = Path.GetFileNameWithoutExtension(file);

            // ugly hard code for format "MM-dd-HH" and "M-dd-HH"
            if (DateFormat[0] == "MM-dd-HH")
            {
                var match = Regex.Match(safefile, @"[0-9]+[^0-9][0-9]+[^0-9][0-9]+");
                if (!match.Success) return false;
                safefile = match.Value;
            } else if (DateFormat[0] == "yyyyMMdd-HH")
            {
                // Hard code II for format "yyyyMMdd-HH"
                var match = Regex.Match(safefile, "\\d+[-]+[0-9][0-9]");
                if (match.Success)
                {
                    safefile = match.Value;
                }
            }

            DateTime date;
            bool succes = DateTime.TryParseExact(safefile, DateFormat, new CultureInfo("da-dk"), DateTimeStyles.None, out date);

            return succes;
        }

        private bool TryGetDateTimeFromFilename(string file, int year, string[] dateformat, out DateTime videodate)
        {
            videodate = new DateTime();
            Console.WriteLine(videodate);

            string safefile = Path.GetFileNameWithoutExtension(file);

            // ugly hard code for format "MM-dd-HH" and "M-dd-HH"
            if (DateFormat[0] == "MM-dd-HH")
            {
                var match = Regex.Match(safefile, @"[0-9]+[^0-9][0-9]+[^0-9][0-9]+");
                if (!match.Success) return false;
                safefile = match.Value;
            } else if (DateFormat[0] == "yyyyMMdd-HH")
            {
                // Hard code II for format "yyyyMMdd-HH"
                var match = Regex.Match(safefile, "\\d+[-]+[0-9][0-9]");
                if (match.Success)
                {
                    safefile = match.Value;
                }
            }

            DateTime tempdate;

            bool succes = DateTime.TryParseExact(safefile, DateFormat, new CultureInfo("da-dk"), DateTimeStyles.None, out tempdate);
            if (succes)
            {
                videodate = new DateTime(year, tempdate.Month, tempdate.Day, tempdate.Hour, tempdate.Minute, tempdate.Second);
            }
            return succes;
        }

        public event EventHandler<ProgressEventArgs> ProgressChanged;

        protected virtual void OnProgressChanged(ProgressEventArgs e)
        {
            EventHandler<ProgressEventArgs> handler = ProgressChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }
}
