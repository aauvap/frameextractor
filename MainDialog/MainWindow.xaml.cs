﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Drawing.Imaging;

using Ookii.Dialogs.Wpf;
using Path = System.IO.Path;
using System.Windows.Threading;
using System.Diagnostics;




namespace MainDialog
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            allFramesYearUpDown.Value = DateTime.Now.Year;

            // populate image format combobox
            var iFSource = EventExtractor.GetSupportedImageFormats();
            imageFormatComboBox.ItemsSource = iFSource;
            imageFormatComboBox.SelectedValuePath = "Format";
            imageFormatComboBox.DisplayMemberPath = "Name";
            imageFormatComboBox.SelectedIndex = 0;

            allFramesImageFormatComboBox.ItemsSource = iFSource;
            allFramesImageFormatComboBox.SelectedValuePath = "Format";
            allFramesImageFormatComboBox.DisplayMemberPath = "Name";
            allFramesImageFormatComboBox.SelectedIndex = 0;

            // populate save interval combobox
            var sSource = ZipInterval.GetAllSupportedZipIntervals();
            saveIntervalComboBox.ItemsSource = sSource;
            //saveIntervalComboBox.SelectedValuePath = "Seconds";
            saveIntervalComboBox.DisplayMemberPath = "Name";
            saveIntervalComboBox.SelectedIndex = 5;

            // populate video format combobox
            var vFSource = EventExtractor.GetSupportedVideoFormats();
            videoFormatComboBox.ItemsSource = vFSource;
            videoFormatComboBox.SelectedValuePath = "ExtensionFilter";
            videoFormatComboBox.DisplayMemberPath = "Name";
            videoFormatComboBox.SelectedIndex = 0;

            //tooltip
            fileDateFormatTextBox.ToolTip = "Year 'y'\nMonth 'M'\nDay 'd'\nHour 'H'\nMinute 'm'\nsecond 's'\nmillisecond 'f'";
            fileDateFormatEvents.ToolTip = "Year 'y'\nMonth 'M'\nDay 'd'\nHour 'H'\nMinute 'm'\nsecond 's'\nmillisecond 'f'";



#if DEBUG
            logPathTextBox.Text = @"D:\Charlotte\TilChris\Logfil_samlet.csv";
            videoPathTextBox.Text = @"D:\Charlotte\TilChris\Video";
            outputPathTextBox.Text = @"D:\Charlotte\TilChris\LogfilSamletFrames";
            secsBeforeUpDown.Value = 1;
            secsAfterUpDown.Value = 1;
#endif

        }

        //*******************************************************
        //
        // Tab: Events
        //
        //*******************************************************

        /// <summary>
        /// Returns file names from given folder that comply to given filters
        /// </summary>
        /// <param name="SourceFolder">Folder with files to retrieve</param>
        /// <param name="Filter">Multiple file filters separated by | character</param>
        /// <param name="searchOption">File.IO.SearchOption, 
        /// could be AllDirectories or TopDirectoryOnly</param>
        /// <returns>Array of FileInfo objects that presents collection of file names that 
        /// meet given filter</returns>
        public string[] getFiles(string SourceFolder, string Filter,
            System.IO.SearchOption searchOption)
        {
            // ArrayList will hold all file names
            System.Collections.ArrayList alFiles = new System.Collections.ArrayList();

            // Create an array of filter string
            string[] MultipleFilters = Filter.Split('|');

            // for each filter find mating file names
            foreach (string FileFilter in MultipleFilters)
            {
                // add found file names to array list
                alFiles.AddRange(Directory.GetFiles(SourceFolder, FileFilter, searchOption));
            }

            // returns string array of relevant file names
            return (string[])alFiles.ToArray(typeof(string));
        }

        EventExtractor Ex;

        private void browseOutputButton_Click(object sender, RoutedEventArgs e)
        {
            VistaFolderBrowserDialog fbd = new VistaFolderBrowserDialog();

            if (fbd.ShowDialog() == true)
            {
                outputPathTextBox.Text = fbd.SelectedPath;
            }
        }

        private void browseVideoButton_Click(object sender, RoutedEventArgs e)
        {
            VistaFolderBrowserDialog fbd = new VistaFolderBrowserDialog();

            if (fbd.ShowDialog() == true)
            {
                videoPathTextBox.Text = fbd.SelectedPath;
                outputPathTextBox.Text = Path.Combine(fbd.SelectedPath, "frames");
            }
        }

        private void loadLogButton_Click(object sender, RoutedEventArgs e)
        {
            VistaOpenFileDialog ofd = new VistaOpenFileDialog();
            ofd.Filter = "csv files (*.csv)|*.csv|all files (*.*)|*.*";

            if (ofd.ShowDialog() == true)
            {
                logPathTextBox.Text = ofd.FileName;
            }
        }

        private void ExtractButton_Click(object sender, RoutedEventArgs e)
        {
            string logfile = logPathTextBox.Text;
            string outputpath = outputPathTextBox.Text;

            if (!File.Exists(logfile))
            {
                MessageBox.Show("The selected log file does not exist.","File not found");
                return;
            }

            string videofilter = (string)videoFormatComboBox.SelectedValue;

            //string[] videofiles = Directory.GetFiles(videoPathTextBox.Text)
            //    .Where(file => videofilter.Any(file.ToLower().EndsWith()
            //    .ToList();

            string[] videofiles;

            try
            {
                videofiles = getFiles(videoPathTextBox.Text, videofilter, SearchOption.AllDirectories);
            } catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error opening video folder");
                return;
            }

            bool saveIncidentsVideoChecked = saveIncidentsVideo.IsChecked ?? false;
            Ex = new EventExtractor(logfile, videofiles, outputpath, saveIncidentsVideoChecked);

            if (secsBeforeUpDown.Value.HasValue) Ex.SecondsBefore = secsBeforeUpDown.Value.Value;
            if (secsAfterUpDown.Value.HasValue) Ex.SecondsAfter = secsAfterUpDown.Value.Value;
            if (imageFormatComboBox.SelectedValue is ImageFormat) Ex.ImageSaveFormat = (ImageFormat)imageFormatComboBox.SelectedValue;

            Ex.DateFormat = fileDateFormatEvents.Text;

            Ex.ProgressChanged += Ex_ProgressChanged;

            Ex.Start();

            stopButton.Visibility = System.Windows.Visibility.Visible;
            ExtractButton.Visibility = System.Windows.Visibility.Collapsed;
        }

        void Ex_ProgressChanged(object sender, ProgressEventArgs e)
        {
            // It is another thread so invoke back to UI thread
            Dispatcher.Invoke((Action)delegate
            {
                if (e.ErrorMessage != null)
                {
                    MessageBox.Show(e.ErrorMessage, "An error occurred");


                } else
                {
                    eventProgressBar.Value = e.Progress;
                    eventStatusMessageLabel.Content = e.StatusMessage;

                    if (!e.Running)
                    {
                        ExtractButton.Visibility = Visibility.Visible;
                        stopButton.Visibility = Visibility.Collapsed;
                    }
                }
            });
        }

        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            if (Ex != null && Ex.IsRunning)
            {
                Ex.Stop();
            }
            eventProgressBar.Value = 0;
            ExtractButton.Visibility = System.Windows.Visibility.Visible;
            stopButton.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (Ex != null && Ex.IsRunning)
            {
                Ex.Stop();
            }
        }

        //*******************************************************
        //
        // Tab: Full Videos
        //
        //*******************************************************

        AllFramesExtractor Ax;

        private void addVideo2ListButton_Click(object sender, RoutedEventArgs e)
        {
            VistaOpenFileDialog ofd = new VistaOpenFileDialog();
            ofd.Multiselect = true;

            if (ofd.ShowDialog() == true)
            {
                string[] files = ofd.FileNames;

                foreach (var file in files)
                {
                    videoListBox.Items.Add(file);
                }
            }
        }

        private void videoListBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                int index = videoListBox.SelectedIndex;

                videoListBox.Items.Remove(videoListBox.SelectedItem);

                while (index + 1 > videoListBox.Items.Count)
                {
                    index--;
                }

                if (index >= 0)
                {
                    videoListBox.SelectedIndex = index;
                }
            }
        }

        private void clearVideoListButton_Click(object sender, RoutedEventArgs e)
        {
            videoListBox.Items.Clear();
        }

        private void extractAllFramesButton_Click(object sender, RoutedEventArgs e)
        {
            Ax = new AllFramesExtractor(videoListBox.Items.Cast<string>().ToList());

            Ax.ProgressChanged += Ax_ProgressChanged;

            if (allFramesFpsUpDown.Value.HasValue) Ax.FPS = allFramesFpsUpDown.Value.Value;
            if (allFramesYearUpDown.Value.HasValue) Ax.YearOfRecording = allFramesYearUpDown.Value.Value;
            if (allFramesImageFormatComboBox.SelectedValue is ImageFormat) Ax.ImageSaveFormat = (ImageFormat)allFramesImageFormatComboBox.SelectedValue;
            if (saveIntervalComboBox.SelectedValue is ZipInterval) Ax.SaveInterval = (ZipInterval)saveIntervalComboBox.SelectedValue;
            if (!String.IsNullOrWhiteSpace(fileDateFormatTextBox.Text))
            {
                string format = fileDateFormatTextBox.Text;
                if (format == "MM-dd-HH") //special case
                {
                    Ax.DateFormat = new string[] { @"MM-dd-HH*", "M-dd-HH", "M-d-HH", "MM-d-HH" };
                }
                else
                {
                    Ax.DateFormat = new string[] { format };
                }
            }

            try
            {
                Ax.Start();
                Debug.WriteLine("All Frame Extractor Started");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void Ax_ProgressChanged(object sender, ProgressEventArgs e)
        {
            if (!e.Running)
            {
                Dispatcher.Invoke(delegate
                {
                    Ax = null;
                    extractAllFramesButton.Visibility = System.Windows.Visibility.Visible;
                    cancelAllFramesButton.Visibility = System.Windows.Visibility.Collapsed;
                });
            }
            else if (e.Running)
            {
                Dispatcher.Invoke(delegate
                {
                    cancelAllFramesButton.Visibility = System.Windows.Visibility.Visible;
                    extractAllFramesButton.Visibility = System.Windows.Visibility.Collapsed;
                });
            }
        }

        private void cancelAllFramesButton_Click(object sender, RoutedEventArgs e)
        {
            if (Ax != null)
            {
                Ax.Stop();
            }
        }
    }
}
