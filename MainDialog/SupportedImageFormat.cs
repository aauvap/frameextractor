﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Imaging;

namespace MainDialog
{
    class SupportedImageFormat
    {
        public string Name { get; set; }
        public ImageFormat Format { get; set; }
    }
}
