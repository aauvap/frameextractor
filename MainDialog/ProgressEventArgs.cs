﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainDialog
{
    class ProgressEventArgs : EventArgs
    {
        public double Progress { get; set; }
        public bool Running { get; set; }
        public string ErrorMessage { get; set; }
        public string StatusMessage { get; set; }
    }
}
