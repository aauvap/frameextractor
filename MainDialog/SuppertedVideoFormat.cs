﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainDialog
{
    class SupportedVideoFormat
    {
        public string Name { get; set; }
        public string ExtensionFilter { get; set; }
    }
}
