﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainDialog
{
    public class ZipInterval
    {
        public string Name { get; private set; }
        public int Seconds { get; private set; }

        /// <summary>
        /// Defaults constructor. Same result as minutes_5()
        /// </summary>
        public ZipInterval()
        {
            Name = "5 Minutes";
            Seconds = 5 * 60;
        }       

        public static ZipInterval seconds_30()
        {
            return new ZipInterval { Name = "30 Seconds", Seconds = 30 };
        }

        public static ZipInterval minutes_1()
        {
            return new ZipInterval { Name = "1 Minute", Seconds = 60 };
        }

        public static ZipInterval minutes_2()
        {
            return new ZipInterval { Name = "2 Minutes", Seconds = 2 * 60 };
        }

        public static ZipInterval minutes_3()
        {
            return new ZipInterval { Name = "3 Minutes", Seconds = 3*60 };
        }

        public static ZipInterval minutes_4()
        {
            return new ZipInterval { Name = "4 Minutes", Seconds = 4*60 };
        }

        public static ZipInterval minutes_5()
        {
            return new ZipInterval { Name = "5 Minutes", Seconds = 5*60 };
        }

        public static ZipInterval minutes_6()
        {
            return new ZipInterval { Name = "6 Minutes", Seconds = 6*60 };
        }

        public static ZipInterval minutes_10()
        {
            return new ZipInterval { Name = "10 Minutes", Seconds = 10*60 };
        }

        public static ZipInterval minutes_15()
        {
            return new ZipInterval { Name = "15 Minutes", Seconds = 15*60 };
        }

        public static ZipInterval minutes_30()
        {
            return new ZipInterval { Name = "30 Minutes", Seconds = 30 * 60 };
        }

        public static ZipInterval minutes_60()
        {
            return new ZipInterval { Name = "60 Minutes", Seconds = 60 * 60 };
        }

        public static List<ZipInterval> GetAllSupportedZipIntervals()
        {
            var ziplist = new List<ZipInterval>();
            ziplist.Add(seconds_30());
            ziplist.Add(minutes_1());
            ziplist.Add(minutes_2());
            ziplist.Add(minutes_3());
            ziplist.Add(minutes_4());
            ziplist.Add(minutes_5());
            ziplist.Add(minutes_6());
            ziplist.Add(minutes_10());
            ziplist.Add(minutes_15());
            ziplist.Add(minutes_30());
            ziplist.Add(minutes_60());
            return ziplist;
        }
    }
}
