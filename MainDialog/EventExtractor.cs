﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;

using Emgu.CV;
using Ionic.Zip;
using System.Threading;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;

/// <summary>
/// Reference Article http://www.codeproject.com/KB/tips/SerializedObjectCloner.aspx
/// Provides a method for performing a deep copy of an object.
/// Binary Serialization is used to perform the copy.
/// </summary>
public static class ObjectCopier
{
    /// <summary>
    /// Perform a deep Copy of the object.
    /// </summary>
    /// <typeparam name="T">The type of object being copied.</typeparam>
    /// <param name="source">The object instance to copy.</param>
    /// <returns>The copied object.</returns>
    public static T Clone<T>(T source)
    {
        if (!typeof(T).IsSerializable)
        {
            throw new ArgumentException("The type must be serializable.", "source");
        }

        // Don't serialize a null object, simply return the default for that object
        if (Object.ReferenceEquals(source, null))
        {
            return default(T);
        }

        IFormatter formatter = new BinaryFormatter();
        Stream stream = new MemoryStream();
        using (stream)
        {
            formatter.Serialize(stream, source);
            stream.Seek(0, SeekOrigin.Begin);
            return (T)formatter.Deserialize(stream);
        }
    }
}

namespace MainDialog
{
    class EventExtractor
    {
        public static List<SupportedImageFormat> GetSupportedImageFormats()
        {
            List<SupportedImageFormat> iFSource = new List<SupportedImageFormat>();
            iFSource.Add(new SupportedImageFormat { Name = "Jpeg", Format = ImageFormat.Jpeg });
            iFSource.Add(new SupportedImageFormat { Name = "Png", Format = ImageFormat.Png });
            iFSource.Add(new SupportedImageFormat { Name = "Bmp", Format = ImageFormat.Bmp });

            return iFSource;
        }

        public static List<SupportedVideoFormat> GetSupportedVideoFormats()
        {
            List<SupportedVideoFormat> vFSource = new List<SupportedVideoFormat>();
            vFSource.Add(new SupportedVideoFormat { Name = "All supported formats", ExtensionFilter = "*.mkv|*.mp4|*.asf" });
            vFSource.Add(new SupportedVideoFormat { Name = "mkv", ExtensionFilter = "*.mkv" });
            vFSource.Add(new SupportedVideoFormat { Name = "asf", ExtensionFilter = "*.asf" });
            vFSource.Add(new SupportedVideoFormat { Name = "mp4", ExtensionFilter = "*.mp4" });

            return vFSource;
        }

        private readonly char[] COMMA_CHAR = new char[] { ',',';' };
        private readonly char[] HYPHEN_CHAR = new char[] { '-' };
        private readonly char[] COLON_CHAR = new char[] { ':', '.' };
        private readonly char[] SPACE_CHAR = new char[] { ' '};
        private readonly char[] UNDERSCORE_CHAR = new char[] { '_' };

        /// <summary>
        /// Time to include before event. Defaults to 5
        /// </summary>
        public int SecondsBefore { get; set; }

        /// <summary>
        /// Time to include after event. Defaults to 5
        /// </summary>
        public int SecondsAfter { get; set; }

        /// <summary>
        /// Year of recording. Defaults to current year
        /// </summary>
        public int YearOfRecording { get; set; }

        /// <summary>
        /// The format of the saved images. Defaults to Jpeg
        /// </summary>
        public ImageFormat ImageSaveFormat { get; set; }

        /// <summary>
        /// The format used to read the timestamps from the input filenames. Defaults to MM-dd-HH
        /// </summary>
        public string DateFormat { get; set; }

        /// <summary>
        /// True if processing, otherwise false
        /// </summary>
        public bool IsRunning { get; private set; }

        private string LogFile { get; set; }
        private string[] VideoFiles { get; set; }
        private string OutputPath { get; set; }
        private bool SaveIncicentsVideo { get; set; }

        CancellationTokenSource tokenSource = new CancellationTokenSource();

        public EventExtractor(string logfile, string[] videofiles, string outputpath, bool saveIndicentsVideo)
        {
            // defaults
            SecondsBefore = 5;
            SecondsAfter = 5;
            ImageSaveFormat = ImageFormat.Jpeg;
            IsRunning = false;

            LogFile = logfile;
            VideoFiles = videofiles;
            OutputPath = outputpath;
            SaveIncicentsVideo = saveIndicentsVideo;
        }

        public void Start()
        {
            OnProgressChanged(new ProgressEventArgs { Progress = 0, Running = true });
            var task = Task.Factory.StartNew(() => ProcessMethod(tokenSource.Token));
            IsRunning = true;
        }

        public void Stop()
        {
            tokenSource.Cancel();
            IsRunning = false;
            OnProgressChanged(new ProgressEventArgs { Progress = 0, Running = false });
        }

        private void ProcessMethod(CancellationToken ct)
        {
            Dictionary<DateTime, string> flagList = GetTimestampsFromLogFile(LogFile);

            if (ct.IsCancellationRequested) return;

            if (!Directory.Exists(OutputPath)) Directory.CreateDirectory(OutputPath);

            if (ct.IsCancellationRequested) return;

            ExtractFrames(flagList, VideoFiles, OutputPath, SecondsBefore, SecondsAfter, ct);

            OnProgressChanged(new ProgressEventArgs { Progress = 100, Running = false, StatusMessage = "Finished!"});
        }

        private void ExtractFrames(Dictionary<DateTime, string> timelist, string[] videofiles, string outputPath, int secsbefore, int secsafter, CancellationToken ct)
        {
            int year = timelist.First().Key.Year;
            int saveLengthInSecs = secsbefore + secsafter;
            //int saveLengthInFrames = saveLengthInSecs * (int)(fps + 0.5);
            TimeSpan before = TimeSpan.FromSeconds(secsbefore);

            Dictionary<DateTime, string> startTimes = new Dictionary<DateTime, string>();

            foreach(var entry in timelist)
            {
                startTimes.Add(entry.Key.Subtract(before), entry.Value);
            }

            //List<DateTime> startTimes = timelist.Select(f => f.Subtract(before)).OrderBy(f => f).ToList();

            // Get all the files to extract
            Dictionary<string, List<KeyValuePair<int, string>>> KeyValueDic = new Dictionary<string, List<KeyValuePair<int, string>>>();

            //var distinctDates = startTimes.Select(f => f.Date).Distinct().ToList();
            var distinctFiles = startTimes.Values.Distinct().ToList();

            int residueFrames = 0;

            // Maximum size of any video frame extracted inside this method
            System.Drawing.Size maxVideoFrameSize = new System.Drawing.Size(0, 0);
            int minVideoFps = 1000;

            VideoFiles = VideoFiles.OrderBy(x => x).ToArray();

            // Create a log file to report errors, if necessary
            try
            {
                var fs = System.IO.File.Create(outputPath + "/" + "timestampsLog.txt");
                fs.Close();
            } catch
            {
                ProgressEventArgs args = new ProgressEventArgs();
                args.Progress = 0;
                args.Running = false;
                args.ErrorMessage = "Error creating log file";

                OnProgressChanged(args);
            }
            

            DateTime lastflag = DateTime.MinValue;
            int fileCount = 0;
            foreach (var file in distinctFiles)
            {
                // get the video for this date and hour

                ProgressEventArgs args = new ProgressEventArgs();
                args.Progress = 100.0 * (double)fileCount / distinctFiles.Count();
                args.Running = true;
                args.StatusMessage = "Building list of timestamps in " + file;
                fileCount++;

                OnProgressChanged(args);

                string videofilename = null;

                try

                {
                    
                    IEnumerable<string> videofilenames = videofiles.Where(f => f.Contains(file));
                    videofilename = videofilenames.OrderBy(name => name.Length).First();

                    if (String.IsNullOrWhiteSpace(videofilename)) continue;

                }
                catch (Exception e)
                {
                    Console.WriteLine("Something is not right ");
                    Debug.WriteLine(e.Message + "\n" + e.Source + "\n" + e.StackTrace + "\n" + e.TargetSite + "\n" + e.Data);

                    try
                    {
                        using (System.IO.StreamWriter tFile =
                            new System.IO.StreamWriter(outputPath + "/" + "timestampsLog.txt", true))
                        {
                            
                            tFile.WriteLine(System.DateTime.Now.ToShortDateString() + " " + System.DateTime.Now.ToShortTimeString() + ": Cannot find video file " + file);
                        }
                    }
                    catch (Exception ex2)
                    {
                        Debug.WriteLine(ex2.Message + "\n" + ex2.Source + "\n" + ex2.StackTrace + "\n" + ex2.TargetSite + "\n" + ex2.Data);

                    }

                    continue;
                }

                DateTime videoStartTime = GetDateTimeFromFilename(file, year);

                Emgu.CV.Capture cap = new Emgu.CV.Capture(videofilename);
                double fps = cap.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps);

                int width = (int)cap.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameWidth);
                int height = (int)cap.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameHeight);

                if ((int)fps < minVideoFps)
                {
                    minVideoFps = (int)fps;
                }

                if (width > maxVideoFrameSize.Width)
                {
                    maxVideoFrameSize.Width = width;
                }

                if (height > maxVideoFrameSize.Height)
                {
                    maxVideoFrameSize.Height = height;
                }

                int videoMaxFrames = 60 * 60 * (int)(fps + 0.5);
                int saveLengthInFrames = saveLengthInSecs * (int)(fps + 0.5);

                List<KeyValuePair<int, string>> frameByKey = new List<KeyValuePair<int, string>>();

                var flagsInVideo = startTimes.Where(pair => pair.Value == file);
                //var flagsInHour = startTimes.Where(f => f.Date.Equals(date)).Where(f => f.Hour.Equals(hour)).OrderBy(f => f).ToList();

                for (int i = 0; i < residueFrames; i++)
                {
                    frameByKey.Add(new KeyValuePair<int, string>(i, lastflag.ToString("MM-dd-HH-mm-ss.fff")));
                }
                residueFrames = 0;

                foreach (var flag in flagsInVideo)
                {
                    TimeSpan timeToStart = flag.Key - videoStartTime;
                    double startMsec = timeToStart.TotalMilliseconds;
                    int startFrame = (int)(startMsec * (fps) / 1000);

                    int frame2save = startFrame;
                    int framesSaved = 0;

                    while (frame2save < videoMaxFrames && framesSaved < saveLengthInFrames)
                    {
                        frameByKey.Add(new KeyValuePair<int, string>(frame2save, flag.Key.ToString("yyyyMMdd-HHmmss-fff")));

                        frame2save++;
                        framesSaved++;
                    }

                    Debug.WriteLine("flag: " + flag.ToString() + "-" + flag.Key.Millisecond);
                    Debug.WriteLine("framesSaved: " + framesSaved);
                    if (framesSaved < saveLengthInFrames)
                    {
                        // This should only happen in the end of a video
                        residueFrames = saveLengthInFrames - framesSaved;
                        Debug.WriteLine("ResidueFrames: " + residueFrames);
                        Debug.WriteLine("flag: " + flag.ToString() + "-" + flag.Key.Millisecond);
                    }

                    lastflag = flag.Key;
                }

                KeyValueDic.Add(Path.GetFileNameWithoutExtension(videofilename), frameByKey);
            }
            

            Dictionary<string, List<KeyValuePair<string, Mat>>> imageFlagDic = new Dictionary<string, List<KeyValuePair<string, Mat>>>();

            var allKeys = KeyValueDic.Values.SelectMany(f => f.Select(g => g.Value)).Distinct().ToList();
            //var allKeys = KeyValueDic.Values.SelectMany(f => f.Select(g => g.Value)).ToList();

            foreach (var key in allKeys)
            {
                imageFlagDic.Add(key, new List<KeyValuePair<string, Mat>>());
                string hourfolder = Path.Combine(outputPath, key.Substring(0, 11));
                if (!Directory.Exists(hourfolder)) Directory.CreateDirectory(hourfolder);
            }

            int totalFlagCount = imageFlagDic.Count;
            int remainingFlagCount = totalFlagCount;
            int lastFlagCount = 0;

            Dictionary<string, int> prevFrameDic = new Dictionary<string, int>();

            var smallDoneList = imageFlagDic.Where(f => f.Value.Count >= 10).ToList();

            foreach (var flag in smallDoneList)
            {
                prevFrameDic[flag.Key] = flag.Value.Count;
            }

            ProgressEventArgs tmpArgs = new ProgressEventArgs();
            tmpArgs.Progress = 0;
            tmpArgs.Running = true;
            tmpArgs.StatusMessage = "Saving frames...";

            OnProgressChanged(tmpArgs);

            VideoWriter vw = null;

            if (SaveIncicentsVideo)
            {
                // Video writer for all frames
                vw = new VideoWriter(outputPath + "//allIncidents.avi", minVideoFps, maxVideoFrameSize, true);
            }

            foreach (var video in videofiles)
            {
                try
                {

                    if (ct.IsCancellationRequested) return;

                    string key = Path.GetFileNameWithoutExtension(video);
                    if (!KeyValueDic.ContainsKey(key)) continue;

                    Dictionary<int, List<string>> frames = new Dictionary<int, List<string>>();

                    foreach (var frame in KeyValueDic[key].Select(f => f.Key).Distinct())
                    {
                        frames.Add(frame, new List<string>());
                        foreach (var incident in KeyValueDic[key].Where(f => f.Key == frame))
                        {
                            frames[frame].Add(incident.Value);
                        }
                    }

                    Debug.WriteLine(key);
                    Emgu.CV.Capture cap = new Emgu.CV.Capture(video);
                    double fps = cap.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps);

                    int videoMaxFrames = 60 * 60 * (int)(fps + 0.5);
                    int saveLengthInFrames = saveLengthInSecs * (int)(fps + 0.5);

                    DateTime videoStartTime = GetDateTimeFromFilename(key, year);
                    DateTime saveTime = videoStartTime;
                    Debug.WriteLine("savetime: " + videoStartTime);

                    int frameNum = 0;

                    while (cap.Grab())
                    {
                        bool exceptionOccured = false;

                        if (ct.IsCancellationRequested)
                        {
                            cap.Dispose();
                            return;
                        }

                        if (frameNum % 50 == 0)
                        {
                            int flagCount = totalFlagCount - remainingFlagCount;
                            if (flagCount != lastFlagCount)
                            {
                                ProgressEventArgs args = new ProgressEventArgs();
                                args.Progress = 100.0 * ((double)flagCount) / ((double)totalFlagCount+1);
                                args.Running = true;
                                args.StatusMessage = "Saving frames in " + video;
                                
                                OnProgressChanged(args);
                                lastFlagCount = flagCount;
                            }
                        }


                        using (Mat image = new Mat())
                        {
                            try
                            {
                                if (cap.Retrieve(image))
                                {
                                    if (frames.ContainsKey(frameNum))
                                    {
                                        Mat tmpImg = image.Clone();

                                        foreach (var incident in frames[frameNum])
                                        {                                          
                                            imageFlagDic[incident].Add(new KeyValuePair<string, Mat>(saveTime.ToString("yyyyMMdd-HHmmss.fff"), image.Clone()));
                                        }

                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                Debug.WriteLine(e.Message + "\n" + e.Source + "\n" + e.StackTrace + "\n" + e.TargetSite + "\n" + e.Data);

                                try
                                {
                                    using (System.IO.StreamWriter tFile =
                                        new System.IO.StreamWriter(outputPath + "/" + "timestampsLog.txt", true))
                                    {
                                        tFile.WriteLine(System.DateTime.Now.ToShortDateString() + " " + System.DateTime.Now.ToShortTimeString() + ": Error adding key " + String.Join("; ", frames[frameNum].ToArray()) + " to imageFlagDic: " + e.Message);
                                        exceptionOccured = true;
                                    }
                                }
                                catch (Exception ex2)
                                {
                                    Debug.WriteLine(ex2.Message + "\n" + ex2.Source + "\n" + ex2.StackTrace + "\n" + ex2.TargetSite + "\n" + ex2.Data);
                                    exceptionOccured = true;
                                }

                            }
                        }

                        frameNum++;
                        saveTime = videoStartTime.AddMilliseconds((frameNum * 1000) / fps);

                        List<string> removeKeys = new List<string>();

                        var doneList = imageFlagDic.Where(f => f.Value.Count >= saveLengthInFrames).ToList();

                        foreach (var flag in doneList)
                        {
                            Debug.WriteLine("saving " + flag.Key);
                            removeKeys.Add(flag.Key);

                            string zipPath = Path.Combine(outputPath, flag.Key.Substring(0, 11), flag.Key + ".zip");
                            SaveZipFile(flag.Value, zipPath, outputPath);

                            if (SaveIncicentsVideo)
                            {
                                SaveVideoFrames(flag.Value, vw, maxVideoFrameSize);
                            }
                        }

                        foreach (var item in removeKeys)
                        {
                            imageFlagDic[item].ForEach(f => f.Value.Dispose());
                            imageFlagDic[item].Clear();
                            remainingFlagCount--;
                        }

                        removeKeys.Clear();

                        smallDoneList = imageFlagDic.Where(f => f.Value.Count >= 10).ToList();

                        foreach (var flag in smallDoneList)
                        {
                            int prevFlagCount = 0;

                            prevFrameDic.TryGetValue(flag.Key, out prevFlagCount);

                            if (prevFlagCount == flag.Value.Count && prevFlagCount > 0 && !exceptionOccured)
                            {
                                Debug.WriteLine("saving " + flag.Value.Count + " frames in " + flag.Key);
                                removeKeys.Add(flag.Key);


                                string zipPath = Path.Combine(outputPath, flag.Key.Substring(0, 11), flag.Key + ".zip");
                                SaveZipFile(flag.Value, zipPath, outputPath);

                                if (SaveIncicentsVideo)
                                {
                                    SaveVideoFrames(flag.Value, vw, maxVideoFrameSize);
                                }
                            }
                        }


                        foreach (var flag in smallDoneList)
                        {
                            prevFrameDic[flag.Key] = flag.Value.Count;
                        }

                        foreach (var item in removeKeys)
                        {
                            imageFlagDic[item].ForEach(f => f.Value.Dispose());
                            imageFlagDic[item].Clear();
                            remainingFlagCount--;
                        }

                        removeKeys.Clear();
                    }

                    cap.Dispose();
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message + "\n" + e.Source + "\n" + e.StackTrace + "\n" + e.TargetSite + "\n" + e.Data);

                    try
                    {
                        using (System.IO.StreamWriter tFile =
                            new System.IO.StreamWriter(outputPath + "/" + "timestampsLog.txt", true))
                        {
                            tFile.WriteLine(System.DateTime.Now.ToShortDateString() + " " + System.DateTime.Now.ToShortTimeString() + ": Error: " + e.Source + e.StackTrace + e.Message);
                        }
                    }
                    catch (Exception ex2)
                    {
                        Debug.WriteLine(ex2.Message + "\n" + ex2.Source + "\n" + ex2.StackTrace + "\n" + ex2.TargetSite + "\n" + ex2.Data);

                    }
                }
            }

            if (SaveIncicentsVideo)
            {
                vw.Dispose();

                ProgressEventArgs args = new ProgressEventArgs();
                args.Progress = 99;
                args.Running = true;
                args.StatusMessage = "Encoding video...";

                OnProgressChanged(args);

                // Convert video to decent codec
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = "cmd.exe";
                string inputFileName = outputPath + "//allIncidents.avi";
                string outputFileName = outputPath + "//allIncidents.mp4";
                startInfo.Arguments = "/C ffmpeg -i " + inputFileName + " -c:v libx264 -preset slow -crf 22 -c:a copy -r " + minVideoFps + outputFileName;
                process.StartInfo = startInfo;
                process.Start();

                process.WaitForExit();

                // Delete the original video file
                //System.IO.File.Delete(inputFileName);
            }

            // Check if the error log file is empty. If not, notify the user
            if (new FileInfo(outputPath + "/" + "timestampsLog.txt").Length > 0)
            {
                ProgressEventArgs errorArgs = new ProgressEventArgs();
                errorArgs.Progress = 100;
                errorArgs.Running = false;
                errorArgs.ErrorMessage = "Some timestamps were not extracted. Check the log file for further details. \n\nThe log file is located at:\n" + outputPath + "/" + "timestampsLog.txt";

                OnProgressChanged(errorArgs);
            }

        }

        private void SaveVideoFrames(List<KeyValuePair<string, Mat>> labeledImages, VideoWriter vWriter, System.Drawing.Size frameSize)
        {
            foreach (var lImage in labeledImages)
            {
                if (lImage.Value.Height < frameSize.Height ||
                    lImage.Value.Width < frameSize.Width)
                {
                    // The dimensions of the image frame does not match the dimensions of the video
                    // Resize the image frame accordingly and write it to the video stream

                    int extraWidth = frameSize.Width - lImage.Value.Width;
                    extraWidth = (extraWidth < 0) ? 0 : extraWidth;

                    int extraHeight = frameSize.Height - lImage.Value.Height;
                    extraHeight = (extraHeight < 0) ? 0 : extraHeight;

                    Emgu.CV.Mat largerFrame = new Emgu.CV.Mat(frameSize, Emgu.CV.CvEnum.DepthType.Cv8U, 3);
                    Emgu.CV.CvInvoke.CopyMakeBorder(lImage.Value, largerFrame, 0, extraHeight, 0, extraWidth, Emgu.CV.CvEnum.BorderType.Constant);

                    vWriter.Write(largerFrame);
                } else
                {
                    vWriter.Write(lImage.Value);
                }

                
            }
        }

        private void SaveZipFile(List<KeyValuePair<string, Mat>> labeledImages, string outputFilename, string outputPath)
        {
            try
            {
                using (ZipFile zipfile = new ZipFile())
                {
                    int imagecount = labeledImages.Count;

                    zipfile.CompressionLevel = Ionic.Zlib.CompressionLevel.Default;
                    zipfile.CompressionMethod = CompressionMethod.Deflate;

                    MemoryStream[] mss = new MemoryStream[imagecount];

                    for (int i = 0; i < imagecount; i++)
                    {
                        string picturename = labeledImages[i].Key + ".jpeg";

                        mss[i] = new MemoryStream();
                        labeledImages[i].Value.Bitmap.Save(mss[i], ImageFormat.Jpeg);
                        mss[i].Seek(0, SeekOrigin.Begin);
                        zipfile.AddEntry(picturename, mss[i]);

                    }
                    zipfile.Save(outputFilename);

                    foreach (var ms in mss)
                    {
                        ms.Dispose();
                    }
                }
            } catch (Exception e)
            {
                Debug.WriteLine(e.Message + "\n" + e.Source + "\n" + e.StackTrace + "\n" + e.TargetSite + "\n" + e.Data);

                try
                {
                    using (System.IO.StreamWriter tFile =
                        new System.IO.StreamWriter(outputPath + "/" + "timestampsLog.txt", true))
                    {
                        tFile.WriteLine(System.DateTime.Now.ToShortDateString() + " " + System.DateTime.Now.ToShortTimeString() + ": Error: " + e.Source + e.StackTrace + e.Message);
                    }
                }
                catch (Exception ex2)
                {
                    Debug.WriteLine(ex2.Message + "\n" + ex2.Source + "\n" + ex2.StackTrace + "\n" + ex2.TargetSite + "\n" + ex2.Data);

                }

                string msg = "Error saving zip file of video frames." +
                    "\nFile: " + outputFilename;
                OnProgressChanged(new ProgressEventArgs { Running = true, ErrorMessage = msg });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename">The filename with path and extension</param>
        /// <returns></returns>
        private DateTime GetDateTimeFromFilename(string safefilename, int year)
        {
            //char[] hyphen = new char[] { '-' };

            //string filename = Path.GetFileNameWithoutExtension(safefilename);
            //string[] fileparts = filename.Split(hyphen);

            //int month, day, hour;

            //if (!Int32.TryParse(fileparts[0], out month)) return DateTime.MinValue;
            //if (!Int32.TryParse(fileparts[1], out day)) return DateTime.MinValue;
            //if (!Int32.TryParse(fileparts[2], out hour)) return DateTime.MinValue;

            DateTime videoDate = new DateTime();// (year, month, day, hour, 0, 0);


            if (!DateTime.TryParseExact(safefilename, DateFormat, System.Globalization.CultureInfo.CurrentCulture,
                System.Globalization.DateTimeStyles.AllowInnerWhite, out videoDate))
            {
                // If this fails, give it a last shot with the use of regular expressions
                List<char[]> splits = new List<char[]>();
                splits.Add(UNDERSCORE_CHAR);
                splits.Add(HYPHEN_CHAR);

                string splitDelimiter = "";

                if (DateFormat.Contains("_"))
                {
                    splitDelimiter = "_";
                } else if (DateFormat.Contains("-"))
                {
                    splitDelimiter = "-";
                }

                Regex r = new Regex("^\\d+" + splitDelimiter + "\\d+");

                Match match = r.Match(safefilename);

                if (match.Success)
                {
                    Debug.WriteLine("Regex used when parsing datetime from filename: Match found:" + match.ToString());

                    if (DateTime.TryParseExact(match.ToString(), DateFormat, System.Globalization.CultureInfo.CurrentCulture,
                    System.Globalization.DateTimeStyles.AllowInnerWhite, out videoDate))
                    {
                        return videoDate;
                    }

                }

                string msg = "Error extracting the start date and time of video file using specified file date format." +
                    "\nVideo file: " + safefilename +
                    "\nSuggested date format: " + DateFormat +
                    "\nPlease change the file date format and try again.";
                OnProgressChanged(new ProgressEventArgs { Running = false, ErrorMessage = msg });
                Debug.WriteLine("Could not get DateTime from file");
            }

            return videoDate;
        }


        private Dictionary<DateTime, string> GetTimestampsFromLogFile(string file)
        {
            //List<DateTime> datetimelist = new List<DateTime>();
            Dictionary<DateTime, string> datetimelist = new Dictionary<DateTime, string>();

            OnProgressChanged(new ProgressEventArgs { Running = true, StatusMessage = "Reading timestamps from " + file });

            try
            {
                string[] lines = File.ReadAllLines(file);

                // first line is comments
                for (int i = 1; i < lines.Length; i++)
                {
                    if (String.IsNullOrWhiteSpace(lines[i])) continue;

                    string[] lineparts = lines[i].Split(COMMA_CHAR);
                    string[] dateparts = lineparts[1].Split(HYPHEN_CHAR);
                    string[] timeparts = lineparts[2].Split(COLON_CHAR);

                    if (dateparts.Count() < 3)
                    {
                        dateparts = lineparts[1].Split(SPACE_CHAR).Reverse().ToArray();

                        string[] tmpParts = timeparts;
                        string[] dateandhour = tmpParts[0].Split(SPACE_CHAR);
                        timeparts[0] = dateandhour.Last();
                    }


                    int year, month, day, hour, minute, second, millisec;

                    if (timeparts.Count() == 4)
                    {
                        if ((Int32.TryParse(dateparts[0], out day) ? 0 : 1) +
                            (Int32.TryParse(dateparts[1], out month) ? 0 : 1) +
                            (Int32.TryParse(dateparts[2], out year) ? 0 : 1) +
                            (Int32.TryParse(timeparts[0], out hour) ? 0 : 1) +
                            (Int32.TryParse(timeparts[1], out minute) ? 0 : 1) +
                            (Int32.TryParse(timeparts[2], out second) ? 0 : 1) +
                            (Int32.TryParse(timeparts[3], out millisec) ? 0 : 1) > 0)
                        {
                            Debug.WriteLine(lines[i]);
                            continue;
                        }
                    } else if (timeparts.Count() == 3)
                    {
                        millisec = 0;

                        if ((Int32.TryParse(dateparts[0], out day) ? 0 : 1) +
                            (Int32.TryParse(dateparts[1], out month) ? 0 : 1) +
                            (Int32.TryParse(dateparts[2], out year) ? 0 : 1) +
                            (Int32.TryParse(timeparts[0], out hour) ? 0 : 1) +
                            (Int32.TryParse(timeparts[1], out minute) ? 0 : 1) +
                            (Int32.TryParse(timeparts[2], out second) ? 0 : 1) > 0)
                        {
                            Debug.WriteLine(lines[i]);
                            continue;
                        }
                    } else
                    {
                        continue;
                    }


                    DateTime timestamp = new DateTime(year, month, day, hour, minute, second, millisec);
                    datetimelist.Add(timestamp, lineparts[0]);
                }
            } catch (Exception e)
            {
                if (datetimelist.Count == 0)
                {
                    string msg = "Error parsing the log file. Could not fetch the timestamps. \n\nError message: \n" + e.Message;
                    OnProgressChanged(new ProgressEventArgs { Running = false, ErrorMessage = msg });
                }
            }

            return datetimelist;
        }

        public event EventHandler<ProgressEventArgs> ProgressChanged;

        protected virtual void OnProgressChanged(ProgressEventArgs e)
        {
            EventHandler<ProgressEventArgs> handler = ProgressChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

    }
}
