#MIT License
#
#Copyright (c) 2016 Aalborg University
#Chris Bahnsen, November 2016
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

import argparse
import numpy as np
import os
from os import listdir
from os.path import isfile, join
import string
import datetime

def evaluateExtractedFolders(csvfile, frameFolder, secondsBefore, resultsFile):
    """
    Compares the entries of the log file (the csvFile) to the extracted frames in the frameFolder, 
    as output by the FrameExtractor program

    Keyword arguments:
    csvFile -- full path to the log file
    frameFolder -- full path to where the output folders are located
    secondsBefore -- seconds before incident for which FrameExtractor has extracted frames
    resultsFile -- full path to the file where the resulting evaluation should be written
    """

    dateHourFolders = {}
    
   # Open the log file
    with open(csvfile) as f:
        for line in f:
            if 'File' not in line and 'Date' not in line:
                entries = line.split(';')

                if len(entries) > 2:
                    logDate = entries[1].split('-') # E.g. dd-MM-HHHH -> [dd MM YYYY]
                    logTime = entries[2].split(':') # E.g. hh:mm:ss.zzz > [hh mm ss.zzz]


                    if len(logDate) > 2 and len(logTime) > 2 :
                        year = int(logDate[2])
                        month = int(logDate[1])
                        date = int(logDate[0])

                        hour = int(logTime[0])
                        minute = int(logTime[1])
                        second = int(logTime[2].split('.')[0])
                        millisecond = int(logTime[2].split('.')[1].replace('\n',''))

                        logDateTime = datetime.datetime(year, month, date, hour, minute, second, millisecond * 1000)
                        startDateTime = logDateTime - datetime.timedelta(seconds=int(secondsBefore))

                        dateHourEntry = startDateTime.strftime('%Y%m%d-%H') # YYYYMMdd-hh

                        if dateHourEntry in dateHourFolders.keys():
                            dateHourFolders[dateHourEntry]['logCount'] += 1
                        else:
                            # Create new entry
                            newEntry = {'logCount': 1, 'fileCount': 0}
                            dateHourFolders[dateHourEntry] = newEntry;


    # Compare the entries in the log file to the actual count of zip files in the frameFolder
    for folderName in dateHourFolders.keys():
        if os.path.isdir(os.path.join(frameFolder, folderName)):
            # Get the number of zip files in this folder

            zipFileCount = 0;

            for file in os.listdir(os.path.join(frameFolder, folderName)):
                if 'zip' in file:
                    zipFileCount += 1

            dateHourFolders[folderName]['fileCount'] = zipFileCount;



    # Write entries to csv file
    f = open(resultsFile, 'w')

    # Write header
    f.write('Folder name;Log count;File count;Difference\n')

    folderNames = dateHourFolders.keys()

    for folderName in sorted(folderNames):
        entry = dateHourFolders[folderName]

        line = folderName + '; ' + str(entry['logCount']) + '; ' + str(entry['fileCount']) + '; ' + str(entry['logCount'] - entry['fileCount']) + '\n'

        f.write(line)

    f.close()


                            




   
parser = argparse.ArgumentParser(description='Compares the entries of the log file (the csvFile) to the extracted frames in the frameFolder', epilog = '')
parser.add_argument('-c', dest = 'csvfile', help = 'full path to the log file')
parser.add_argument('-f', dest = 'frameFolder', help = 'full path to where the output folders are located')
parser.add_argument('-s', dest = 'secondsBefore', help = 'seconds before incident for which FrameExtractor has extracted frames')
parser.add_argument('-r', dest = 'resultsFile', help = 'full path to the file where the resulting evaluation should be written')


args = parser.parse_args()

csvFile = args.csvfile
frameFolder = args.frameFolder
secondsBefore = args.secondsBefore
resultsFile = args.resultsFile



evaluateExtractedFolders(csvFile, frameFolder, secondsBefore, resultsFile)