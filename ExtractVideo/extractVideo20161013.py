#MIT License
#
#Copyright (c) [2016] [Aalborg University]
#Chris Bahnsen, June 2016
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

import argparse
import numpy as np
import os
from os import listdir
from os.path import isfile, join
import string
import cv2
import zipfile
from time import sleep


def extractVideo(baseVideoFolder, fps, frameHeight, frameWidth):
    """
    Operates on the output folders of FrameExtractor and creates a video of the extracted frames

    Keyword arguments:
    baseVideoFolder -- full path containing the base directory where FrameExtractor outputs folders
    videoDest -- full path to where the resulting video file should be stored
    """
    fourcc = cv2.VideoWriter_fourcc('m', 'p','g','2')
    vWriterAll = cv2.VideoWriter(os.path.join(baseVideoFolder, 'video.avi'), fourcc, float(fps), (int(frameWidth), int(frameHeight)), 1)

    # Open all the folders in the originFolder and see which contain suitable annotations
    for dir in os.listdir(baseVideoFolder):
        if os.path.isdir(os.path.join(baseVideoFolder, dir)):
            vWriterFolder = cv2.VideoWriter(os.path.join(baseVideoFolder, dir, 'video.avi'), fourcc, float(fps), (int(frameWidth), int(frameHeight)), 1)

            for zip in os.listdir(os.path.join(baseVideoFolder, dir)):

                fileList = []

                if not os.path.exists('tmp'):
                    os.makedirs('tmp')

                if '.zip' in zip:
                    archive = zipfile.ZipFile(os.path.join(baseVideoFolder, dir, zip), 'r')
                    prevImagePath = ""

                    for imagePath in archive.namelist():
                        extractedImagePath = archive.extract(imagePath, 'tmp')
                        # imageData = archive.read(imagePath) 

                        image = cv2.imread(extractedImagePath)

                        vWriterAll.write(image)
                        vWriterFolder.write(image)

                        fileList.append(imagePath)
                        
                    archive.close()
                    sleep(0.01) # Sleep 10 milliseconds

                for file in fileList:
                    os.remove(os.path.join('tmp',file))
                        

                            

            vWriterFolder.release()
       

    vWriterAll.release()
    print('Finished!');

   
parser = argparse.ArgumentParser(description='The program extract image frames as saved by FrameExtractor and saves them in a single video file', epilog = '')
parser.add_argument('-b', dest = 'baseVideoFolder', help = 'full path containing the base directory of the folders')
parser.add_argument('-f', dest = 'fps', help = 'frame rate of the video file')
parser.add_argument('-e', dest = 'frameHeight', help = 'height of the video frame')
parser.add_argument('-w', dest = 'frameWidth', help = 'width of the video frame')


args = parser.parse_args()

baseVideoFolder = args.baseVideoFolder
fps = args.fps
frameHeight = args.frameHeight
frameWidth = args.frameWidth



extractVideo(baseVideoFolder, fps, frameHeight, frameWidth)